package com.eightbitplatoon.jmsqueuemessageconsumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;

/**
 * @author Benny
 *
 */
@SpringBootApplication
@ImportResource("classpath:Beans.xml")
public class ApacheRouterApplication {

	/**
	 * @param args The argument
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
        SpringApplication.run(ApacheRouterApplication.class, args);
    }
}
