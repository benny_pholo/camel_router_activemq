package com.eightbitplatoon.jmsqueuemessageconsumer;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.ProducerTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.eightbitplatoon.data.MessageJSONReader;
import com.eightbitplatoon.data.MessageXMLMarshaler;
import com.eightbitplatoon.model.Message;

/**
 * @author Benny
 *
 */
public class MyBean implements Processor {

	/**
	 * Class logger.
	 */
	private static Logger log = LoggerFactory.getLogger(MyBean.class);

	/**
	 * Get the message body and covert it to xml and sent it to ActiveMQ queue.
	 *
	 * @param exchange
	 *            The exchange
	 */
	public void process(final Exchange exchange) {
		try {
			String msg = exchange.getIn().getBody().toString();
			log.info(msg);
			MessageJSONReader mJSON = new MessageJSONReader();
			Message msgObj = new Message();
			MessageXMLMarshaler mXML = new MessageXMLMarshaler();

			// validate and mashal json message to a object
			msgObj = mJSON.readJSONData(msg);

			// Marshal message obj to a XML
			String xmlMessage = mXML.convertToXML(msgObj);

			// Pass the xml mess to a ActiveMQ
			log.debug("Sending message to ActiveMQ with the queue name in");
			ProducerTemplate template = exchange.getContext().createProducerTemplate();
			template.sendBody("jms:queue:in", xmlMessage);

			log.info("Message has been send");
			log.info("**********************END**************************");
		} catch (Exception ex) {
			log.error(ex.getMessage());
		}
	}
}
