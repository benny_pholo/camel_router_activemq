package com.eightbitplatoon.tests;

import static org.junit.Assert.assertEquals;
import org.junit.Test;
import com.eightbitplatoon.data.MessageJSONReader;
import com.eightbitplatoon.data.MessageXMLMarshaler;
import com.eightbitplatoon.model.Message;

/**
 *
 * @author Benny.
 *
 */
public class ApplicationTest {

	/**
	 *
	 */
	@Test
	public void testMessageJSONReader() {
		try {
		MessageJSONReader mjr = new MessageJSONReader();
		Message msgActual = new Message();
		msgActual.setId(1);
		msgActual.setMessage("Test");
		String jsonData = "{\"id\":1,\"message\":\"Test\"}";
		Message msgExpected = mjr.readJSONData(jsonData);
		System.out.println("Expected:" + msgExpected.toString());
		System.out.println("Actual:" + msgActual.toString());
		assertEquals(msgExpected.toString(), msgActual.toString());
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
	}

	/**
	 * Testing XML marshal.
	 */
	@Test
	public void testMessageXMLMarshal() {
		MessageXMLMarshaler mxc = new MessageXMLMarshaler();
		Message msgObj = new Message();
		msgObj.setId(1);
		msgObj.setMessage("Test");
		String xmlExpected = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?> <message> "
				+ "<id>1</id> <message>Test</message> </message>";
		try {
		String xmlActual = mxc.convertToXML(msgObj);
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		//assertEquals(xmlExpected, xmlActual);
	}

}
