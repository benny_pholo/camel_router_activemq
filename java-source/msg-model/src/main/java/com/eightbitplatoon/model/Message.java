package com.eightbitplatoon.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


/**
 *
 * @author Benny.
 *
 */
@XmlRootElement(name = "ActiveMQ")
public class Message {

	/**
	 * id.
	 */
	private int id;
	/**
	 * message.
	 */
	private String message;

	/**
	 * Message public constructor.
	 */
	public Message() {

	}

	/**
	 * Message parameterized constructor.
	 * @param id The id
	 * @param message The message
	 */
	@SuppressWarnings("unused")
	private Message(int id, String message) {
		super();
		this.id = id;
		this.message = message;
	}


	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	@XmlElement
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message the message to set.
	 */
	@XmlElement
	public void setMessage(String message) {
		this.message = message;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Message [id=" + id + ", message=" + message + "]";
	}
}
