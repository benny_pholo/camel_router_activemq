package com.eightbitplatoon.data;

import java.io.IOException;

import com.eightbitplatoon.model.Message;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 *
 * @author Benny.
 *
 */
public class MessageJSONReader {

	/**
	 * This method convert JSON string to Message data Object.
	 * @param jsonData JSON data
	 * @return the Message object data
	 * @throws Exception The Exception
	 */
	public Message readJSONData(final String jsonData) throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		Message msg = new Message();
		try {
		 msg = mapper.readValue(jsonData, Message.class);

		} catch (JsonMappingException ex) {
			throw new Exception(ex.getMessage());
		} catch (JsonParseException ex) {
			throw new Exception(ex.getMessage());
		} catch (IOException ex) {
			throw new Exception(ex.getMessage());
		}
		return  msg;

	}
}
