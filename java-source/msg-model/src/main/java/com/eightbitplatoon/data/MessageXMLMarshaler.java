package com.eightbitplatoon.data;


import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import com.eightbitplatoon.model.Message;
/**
 *
 * @author Benny
 *
 */
public class MessageXMLMarshaler {

	/**
	 * This method marshal Message objcect an XML.
	 * @param messageObj The message Object
	 * @return xmlString
	 * @throws Exception The exception
	 */
	public String convertToXML(final Message messageObj) throws Exception {
		StringWriter sw = new StringWriter();
		try {
			JAXBContext jaxbContex = JAXBContext.newInstance(Message.class);
			Marshaller jaxbMashaller = jaxbContex.createMarshaller();
			//output formated xml
			jaxbMashaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			jaxbMashaller.marshal(messageObj, sw);
			//System.out.println(sw.toString());
		} catch (JAXBException e) {
			throw new Exception(e.getMessage());
		}
		return sw.toString();
	}
}
