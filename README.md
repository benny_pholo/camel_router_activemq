# Camel Router ActiveMQ

The project is meant to consume message queues from ActiveMQ transform them to XML and sent them to ActiveMQ.


### Prerequisites ###
Running these deployment examples requires you to have the following softwares installed

* Java JDK 7 or Java JDK 8
* Maven 3
* ActiveMQ
* Eclipse (JBoss Developer Studio is used)

### Building the application ###
Navigate to **[Repos]/Camel_Router_ActiveMQ/java-source** and run the below maven script
```shell
mvn clean install
```
### Starting ActiveMQ server using command prompt/terminal ###
Navigate to your activeMQ directory
 
**[..dir]/apache-activemq-5.15.0/bin/**

* Open the terminal or command prompt
* Type: ./activemq start

Then the Infomation below will be shown:
```shell
INFO: Loading '/home/svc_dev/Servers/apache-activemq-5.15.0//bin/env'
INFO: Using java '/usr/bin/java'
INFO: Starting - inspect logfiles specified in logging.properties and log4j.properties to get details
INFO: pidfile created : '/home/svc_dev/Servers/apache-activemq-5.15.0//data/activemq.pid' (pid '9170')
```
* Type: ./activemq status

You should see ActiveMQ is running (pid '9170')

* Now open the browser and type http://localhost:8161/

You should see ActiveMQ browser 
* open activeMQ admin browser by typing http://localhost:8161/admin
* enter the username and password
* default should be admin:admin

### Runing the application to connect with activeMQ ###
Run the jar file in the command

* Navigate to **[Repos]/Camel_Router_ActiveMQ/java-source/JMSQueueMessageConsumer/target** using command promt/terminal
```shell
java -jar JMSQueueMessageConsumer-0.0.1-SNAPSHOT.jar
```
You should see last line being like this :

Apache Camel 2.16.3 (CamelContext: camel-1) started in 0.752 seconds

Now Spring application will start watching the ActiveMQ 

### Testing the application ###
* Open activeMQ admin browser by typing http://localhost:8161/admin
* Once logon click Send tab
* On the Destination field type: out
* On Queue or Topic dropdown select Queue
* On Number of messages to send field leave it as 1 
* On Message body message box type:
```shell
{ "id":2017,"message":"Testing message" }
```
* and click send button

To check the xml that was send back to activeMQ queue

* Click in from the queue table
* click message id appearing under Message ID in the table
* scroll down to Message Details, you should see a message with xml formart.

### Logs ###
Location: **[Repos]/Camel_Router_ActiveMQ/java-source/JMSQueueMessageConsumer/target/system.log **

### Configurations ###
- TBD
### Deployment instructions ###
- TBD

### Who do I talk to? ###
Benny Pholo
